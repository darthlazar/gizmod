#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>

#include "../allvars.h"
#include "../proto.h"
#include "../kernel.h"

#define GSLWORKSIZE 100000

/*! \file sidm_routines.c
 *  \brief Fuctions and routines needed for the calculations of dark matter self interactions
 *
 *  This file contains the functions and routines necesary for the computation of
 *  the self-interaction probabilities and the velocity kicks due to the interactios.
 *  Originally written by Miguel Rocha, rocham@uci.edu. Oct 2010. Updated on 2014 & re-written by PFH March 2018
 */

/*! This function calculates the interaction probability between two particles.
 *  It checks if comoving integration is on and does the necesary change of
 *  variables and units.
 */

#ifdef DM_SIDM


double prob_of_interaction(double mass, double r, double h_si, double dV[3], double dt)
{
    double dVmag = sqrt(dV[0]*dV[0]+dV[1]*dV[1]+dV[2]*dV[2]) / All.cf_atime; // velocity in physical
    double rho_eff = mass / (h_si*h_si*h_si) * All.cf_a3inv; // density in physical
    double cx_eff = All.DM_InteractionCrossSection * g_geo(r/h_si); // effective cross section (physical) scaled to cgs
    double units = UNIT_SURFDEN_IN_CGS; // needed to convert everything to cgs
    if(All.DM_InteractionVelocityScale>0) {double x=dVmag/All.DM_InteractionVelocityScale; cx_eff/=1+x*x*x*x;} // take velocity dependence
    return rho_eff * cx_eff * dVmag * dt * units; // dimensionless probability
}
/*! This routine sets the kicks for each particle after it has been decided that they will
 *  interact. It uses an algorithm tha conserves energy and momentum but picks a random direction so it does not conserves angular momentum. */
#if !defined(GRAIN_COLLISIONS) /* if using the 'grain collisions' module, these functions will be defined elsewhere [in the grains subroutines] */
void calculate_interact_kick(double dV[3], double kick[3], double m)
{
    double dVmag = (1-All.DM_DissipationFactor)*sqrt(dV[0]*dV[0]+dV[1]*dV[1]+dV[2]*dV[2]);
    if(dVmag<0) {dVmag=0;}
    if(All.DM_KickPerCollision>0) {double v0=All.DM_KickPerCollision; dVmag=sqrt(dVmag*dVmag+v0*v0);}
    double cos_theta = 2.0*gsl_rng_uniform(random_generator)-1.0, sin_theta = sqrt(1.-cos_theta*cos_theta), phi = gsl_rng_uniform(random_generator)*2.0*M_PI;
    kick[0] = 0.5*(dV[0] + dVmag*sin_theta*cos(phi));
    kick[1] = 0.5*(dV[1] + dVmag*sin_theta*sin(phi));
    kick[2] = 0.5*(dV[2] + dVmag*cos_theta);
}

#ifdef DM_SIDM_ANALYTIC_HOST
    #include "dispersion_profile.h"
    #include <gsl/gsl_interp2d.h>

    /*! This routine takes an input parameter (velocity) and a void pointer containing two doubles A and B. We cast the pointer as a struct of 
    *   type int_params, and return the value of the integrand defined in equation 22
    */
    double integrand(double x, void *p) {
        struct int_params *params = (struct int_params *)p;
        return g_func(params->A*sqrt(x))/g_func(params->A) * 1.0/sqrt((1 + params->B - x)*(x + params->B - 1));
    }

    /*! This routine takes an input parameter (velocity) and return the calculation of the function g defined in equation 14 accounting for velocity scaling
    */
    double g_func(double x) {
        if(All.DM_InteractionVelocityScale>0) {double x = x/All.DM_InteractionVelocityScale;}
        return x*All.DM_InteractionCrossSection/(1 + x*x*x*x);
    }

    /*! This routine takes no inputs, instead it draws on constants defined in "dispersion.h" to create a 2D interpolation grid of size A_res*B_res.
    *   We loop through each point in the 2D grid, covering all points (A, B) for (B_min < B < B_max) and (A_min < A < A_max). We then popuate each such
    *   point with the corresponding value of the integral in equation 22. This function is run only once at runtime (it is called in begrun.c:228) and
    *   the values are stored in sh_interp2d_integral.
    */
    void init_interp_grid()
    {
        A_pts = malloc(A_res*sizeof(double));
        B_pts = malloc(B_res*sizeof(double));
        I_pts = malloc(A_res*B_res*sizeof(double));
        for(int i = 0; i < A_res; i++) {*(A_pts + i) = i*((double)A_max/A_res);} /* Define the set of A_pts as a function of A_max/A_res */
        for(int j = 0; j < B_res; j++) {*(B_pts + j) = j*((double)B_max/B_res);} /* Define the set of B_pts as a function of B_max/B_res */
        sh_interp2d_integral = gsl_interp2d_alloc(gsl_interp2d_bilinear, A_res, B_res); /* Allocate memory to the interpolation grid */

        A_interp_acc = gsl_interp_accel_alloc();
        B_interp_acc = gsl_interp_accel_alloc();
        
        gsl_function F;
        F.function = &integrand;
        struct int_params params;

        /* Begin nested for loop through all points A and B*/
        for(size_t i = 1; i < A_res; i++) {
            for(size_t j = 1; j < B_res; j++) {
            
            params.A = *(A_pts + i);
            params.B = *(B_pts + j);
            
            /*! Set the params variable of gsl_function F to a void pointer corresponding to the address of the params structure*/
            F.params = (void *)&params;
            
            /*! Declare and initialize variables needed by gsl_integration_qng functions.
            *   neval: Number of iterations before finding a solution
            *   result: Double storing result of integration calculation, to be stored in grid
            *   error: The error of the calculation
            *   lbound, ubound: Limits of integration
            */
            size_t neval;
            double result, error;
            double lbound = 1 - *(B_pts + j);
            double ubound = 1 + *(B_pts + j);

            /* Calculate the integral and store the value in result */
            int code = gsl_integration_qng(&F, lbound, ubound, epsabs, epsrel, &result, &error, &neval);
            /* Add the integral times g(A)/Pi to the interpolation grid at position (i,j) */
            gsl_interp2d_set(sh_interp2d_integral, I_pts, i, j, g_func(params.A)*result/M_PI); /* Include \Delta{vr}\Delta{vt} constant */
            }
        }
        gsl_interp2d_init(sh_interp2d_integral, A_pts, B_pts, I_pts, A_res, B_res); /* Initialize the interpolation grid */
    }

    /*! This routine takes two doubles A and B as inputs and returns a double for the result of the integral defined in equation 22,
    *   based on those two parameters.
    */
    double integral_lookup(double A, double B) {
        /* If the values A and B passed are within the interpolation grid, simply evaluate them with gsl_interp2d_eval and return the result */
        if((A > A_min && A < A_max ) && (B > B_min && B < B_max )) {
            return gsl_interp2d_eval(sh_interp2d_integral, A_pts, B_pts, I_pts, A, B, A_interp_acc, B_interp_acc);
        }
        /* If A < A_min, we need to create to solve for the analytic solution of the integral. */
        else if((B > B_min && B < B_max ) && A <= A_min) {

        }
        /* If A and B are outside the range (B_min, B_max) or A > A_max, then return 0. In the first case this is non-physical and should
        *  result in 0. In the event A > A_max, the function approaches 0 and is well approximated by it. */
        else{return 0;}
    }

    /*! This function calculates the density of an NFW profile (with Milky Way properties) as a function of radius, 
    *   given as the input parameter r
    */
    double NFW_density_calc(double r) {
        double x=r/SH_Rs;
        return SH_p_const/(x*(1+x)*(1+x));
    }

    /*! This function calculates the interaction probability between a particle and an analytic host. We calculate all the necessary 
    *   coordinate transformations, then, return a dimensionless double representing the probability of interaction with the host
    */
    double prob_of_host_interaction(double mass, double r, double V[3], double Pos[3], double dt)
    {
        double Vmag = sqrt(V[0]*V[0]+V[1]*V[1]+V[2]*V[2]) / All.cf_atime; /* Velocity of the particle in physical units */
        double theta = acos(Pos[2]/r), phi = atan(Pos[1]/Pos[0]);
        double vs2 = Vmag*Vmag;
        double vsr = V[0]*sin(theta)*cos(phi) + cos(theta)*(V[1]*sin(phi) + V[2]);
        double vst = sqrt((V[0]*cos(theta)*cos(phi) + V[1]*cos(theta)*sin(phi) - V[2]*sin(theta))*(V[0]*cos(theta)*cos(phi) + V[1]*cos(theta)*sin(phi) - V[2]*sin(theta)) + (V[0]*sin(phi)-V[1]*cos(phi))*(V[0]*sin(phi)-V[1]*cos(phi)));
        double vr = 0, vt = 0; /*NEED TO FIX*/
        /*Calculate only once*/
        double v2 = vr*vr + vt*vt;
        /*Quadrature loop--keep integrals--should average over several bins*/
        double A = sqrt(v2 + vs2 -2*vr*vsr), B = 2*vt*vst/A; /* Calculate the values of A and B */
        double I_val = integral_lookup(A, B); /* Pass the calculated values of A and B to our integral lookup function */
        double dagger = 0; /*NEED TO FIX*/
        double NFW_rho = NFW_density_calc(r) * All.cf_a3inv; /* Calculate halo density based on particle position */
        double units = UNIT_SURFDEN_IN_CGS; /* Needed to convert everything to cgs */
        if(All.DM_InteractionVelocityScale>0) {double x=Vmag/All.DM_InteractionVelocityScale;} /* Velocity dependence */
        return dt * NFW_rho * dagger * I_val * units; /* Dimensionless probability */
    }
#endif

#endif


/*! This function returns the value of the geometrical factor needed for the calculation of the interaction probability. */
double g_geo(double r)
{
    double f, u; int i; u = r / 2.0 * GEOFACTOR_TABLE_LENGTH; i = (int) u;
    if(i >= GEOFACTOR_TABLE_LENGTH) {i = GEOFACTOR_TABLE_LENGTH - 1;}
    if(i <= 1) {f = 0.992318  + (GeoFactorTable[0] - 0.992318)*u;} else {f = GeoFactorTable[i - 1] + (GeoFactorTable[i] - GeoFactorTable[i - 1]) * (u - i);}
    return f;
}

/*! This routine initializes the table that will be used to get the geometrical factor
 *  as a function of the two particle separations. It populates a table with the results of the numerical integration */
void init_geofactor_table(void)
{
    int i; double result, abserr,r;
    gsl_function F; gsl_integration_workspace *workspace; workspace = gsl_integration_workspace_alloc(GSLWORKSIZE);
    for(i = 0; i < GEOFACTOR_TABLE_LENGTH; i++)
    {
        r =  2.0/GEOFACTOR_TABLE_LENGTH * (i + 1);
        F.function = &geofactor_integ;
        F.params = &r;
        gsl_integration_qag(&F, 0.0, 1.0, 0, 1.0e-8, GSLWORKSIZE, GSL_INTEG_GAUSS41,workspace, &result, &abserr);
        GeoFactorTable[i] = 2*M_PI*result;
    }
    gsl_integration_workspace_free(workspace);
}

/*! This function returns the integrand of the numerical integration done on init_geofactor_table(). */
double geofactor_integ(double x, void * params)
{
    double result, abserr, r, newparams[2];
    r = *(double *) params; newparams[0] = r; newparams[1] = x;
    gsl_function F; gsl_integration_workspace *workspace; workspace = gsl_integration_workspace_alloc(GSLWORKSIZE);
    F.function = &geofactor_angle_integ; F.params = newparams;
    
    gsl_integration_qag(&F, -1.0, 1.0, 0, 1.0e-8, GSLWORKSIZE, GSL_INTEG_GAUSS41,workspace, &result, &abserr);
    gsl_integration_workspace_free(workspace);
    
    /*! This function returns the value W(x). The values of the density kernel as a funtion of x=r/h */
    double wk=0; if(x<1) kernel_main(x, 1, 1, &wk, &wk, -1);
    return x*x*wk*result;
}

/*! This function returns the integrand of the angular part of the integral done on init_geofactor_table(). */
double geofactor_angle_integ(double u, void * params)
{
    double x,r,f;
    r = *(double *) params;
    x = *(double *) (params + sizeof(double));
    f = sqrt(x*x + r*r + 2*x*r*u);
    double wk=0; if(f<1) kernel_main(f, 1, 1, &wk, &wk, -1); /*! This function returns the value W(x). The values of the density kernel as a funtion of x=r/h */
    return wk;
}

/*! This function simply initializes some variables to prevent memory errors */
void init_self_interactions() {int i; for(i = 0; i < NumPart; i++) {P[i].dtime_sidm = 0; P[i].NInteractions = 0;}}

#endif
